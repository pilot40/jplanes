package jplanes;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;


public class SampleController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button c206_btn;

    @FXML
    private Button c208_btn;

    @FXML
    private Button pc12_btn;

    @FXML
    private AnchorPane start_panel;


    @FXML
    void initialize() {
        c208_btn.setOnAction(actionEvent -> {
            //System.out.println("C208");
            c208_btn.getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/jplanes/c208.fxml"));
            try {
                loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setTitle("Расчеты для Cessna208B");
            stage.setResizable(false);
            stage.showAndWait();

        });
        c206_btn.setOnAction(actionEvent -> {
            System.out.println("C206");

        });
        pc12_btn.setOnAction(actionEvent -> {
            System.out.println("PC-12");

        });


    }

}