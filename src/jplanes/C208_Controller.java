package jplanes;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.io.IOException;

public class C208_Controller {

    @FXML
    private Tab tabfuel;

    @FXML
    private Font x1;

    @FXML
    private Button c208_topl_rassch_btn;

    @FXML
    private TextField c208_rasst_tx;

    @FXML
    private TextField c208_zap_tx;

    @FXML
    private TextField c208_speed_tx;

    @FXML
    private TextField c208_topl_gob_tx;

    @FXML
    private TextField c208_topl_pol_tx;

    @FXML
    private TextField c208_topl_zap_tx;

    @FXML
    private Button c208_topl_nazad_btn;

    @FXML
    private Tab tabwind;

    @FXML
    private Tab tab67422_pass;

    @FXML
    private Tab tab67422_sz1;

    @FXML
    private Tab tab67422_sz2;

    @FXML
    private Tab tab67720_pass;

    @FXML
    private Tab tab67720_sz1;

    @FXML
    private Tab tab67720_sz2;

    @FXML
    private Tab tab67450_pass;

    @FXML
    private Tab tab67450_sz1;

    @FXML
    private Tab tab67450_sz2;



    @FXML
    void initialize() {
        c208_topl_nazad_btn.setOnAction(actionEvent -> {
            //System.out.println("C208");
            c208_topl_nazad_btn.getScene().getWindow().hide();

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/jplanes/sample.fxml"));
            try {
                loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root, 350, 393));
            stage.setTitle("Самолёты Аэрогео");
            stage.setResizable(false);
            stage.show();

        });



    }

}
